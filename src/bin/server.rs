use clap::Parser;
use network_reservations::protocol::{Message, Protocol};
use network_reservations::ResourceCount;

use network_reservations::client_messages::{ReservationRequest, ReservationResponse};
use std::net::{SocketAddr, TcpListener};

#[derive(Parser, Debug)]
#[clap(author, version, about)]
pub struct ServerOpts {
    #[clap(long)]
    pub identifier: u64,
    #[clap(long)]
    pub tcp_port: u16,
    #[clap(long)]
    pub gateway: Option<SocketAddr>,
    pub resources: Vec<ResourceCount>,
}

fn main() {
    let opts = ServerOpts::parse();

    let listener = TcpListener::bind(("127.0.0.1", opts.tcp_port)).unwrap();
    for stream in listener.incoming() {
        let stream = stream.unwrap();
        println!("New connection from {}", stream.peer_addr().unwrap());
        let mut protocol = Protocol::from_tcp_stream(stream, opts.identifier);

        let message: Message<ReservationRequest> = protocol.recv().unwrap();
        println!("Received reservation request: {:?}", message.payload);
        let response = ReservationResponse::Failure;
        protocol.send(response).unwrap();
    }
}
