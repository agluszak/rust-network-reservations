use clap::Parser;
use network_reservations::client_messages::{ReservationRequest, ReservationResponse};
use network_reservations::protocol::Protocol;
use network_reservations::ResourceCount;
use std::net::SocketAddr;

#[derive(Parser, Debug)]
#[clap(author, version, about)]
pub struct ClientOpts {
    #[clap(long)]
    pub identifier: u64,
    #[clap(long)]
    pub gateway: SocketAddr,
    pub resources: Vec<ResourceCount>,
}

fn main() {
    let opts = ClientOpts::parse();

    let mut protocol = Protocol::connect_tcp(opts.gateway, opts.identifier).unwrap();

    let request = ReservationRequest(opts.resources.into());

    protocol.send(request).unwrap();
    let message = protocol.recv().unwrap();
    let response: ReservationResponse = message.payload;
    println!("{:?}", response);
}
