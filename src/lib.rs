use anyhow::{anyhow, ensure};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::str::FromStr;

pub mod client_messages;
pub mod main_messages;
pub mod main_server;
pub mod protocol;
pub mod worker_server;

#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq, Deserialize, Serialize)]
pub struct Resource(char);

impl FromStr for Resource {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        ensure!(s.len() == 1, "resource must be a single character");
        let c = s
            .chars()
            .next()
            .ok_or_else(|| anyhow!("missing resource"))?;
        ensure!(c.is_ascii_alphabetic(), "resource must be alphabetic");
        Ok(Resource(c))
    }
}

#[derive(Debug, Clone)]
pub struct ResourceCount(pub Resource, pub u64);

impl FromStr for ResourceCount {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut parts = s.split(':');
        let resource = parts.next().ok_or_else(|| anyhow!("missing resource"))?;
        let count = parts.next().ok_or_else(|| anyhow!("missing count"))?;
        ensure!(parts.next().is_none(), "too many parts");
        Ok(ResourceCount(resource.parse()?, count.parse()?))
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Resources(HashMap<Resource, u64>);

impl Resources {
    pub fn new() -> Self {
        Resources(HashMap::new())
    }

    pub fn add(&mut self, resources: &Resources) {
        for (resource, count) in resources.0.iter() {
            *self.0.entry(*resource).or_insert(0) += count;
        }
    }
}

impl From<Vec<ResourceCount>> for Resources {
    fn from(counts: Vec<ResourceCount>) -> Self {
        let mut map = HashMap::new();
        for count in counts {
            map.insert(count.0, count.1);
        }
        Resources(map)
    }
}

impl Resources {
    /// Check if the resources in `other` are a subset of the resources in `self`.
    pub fn is_subset(&self, other: &Resources) -> bool {
        for (resource, count) in other.0.iter() {
            if self.0.get(resource).map_or(false, |c| *c < *count) {
                return false;
            }
        }
        true
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct ServerId(pub u64);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct ClientId(pub u64);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct TransactionId(pub u64);
