use crate::{ClientId, Resources, ServerId, TransactionId};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::net::SocketAddr;
use thiserror::Error;

#[derive(Debug, Serialize, Deserialize)]
pub enum MainRequestType {
    Reservation(ReservationRequest),
    ConnectWorker(ConnectWorkerRequest),
    ReservationConfirmation(ReservationConfirmation),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ReservationConfirmation {
    pub transaction_id: TransactionId,
    pub server_id: ServerId,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ReservationRequest {
    pub reply_to: ServerId,
    pub client: ClientId,
    pub resources: Resources,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ConnectWorkerRequest {
    pub reply_to: ServerId,
    pub worker_id: ServerId,
    pub address: SocketAddr,
    pub resources: Resources,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum MainResponseType {
    Reservation(Result<ReservationResponse, ReservationError>),
    ConnectWorker(Result<ConnectWorkerResponse, ConnectWorkerError>),
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ReservationResponse(HashMap<ServerId, Resources>);

#[derive(Debug, Serialize, Deserialize, Error)]
pub enum ReservationError {
    #[error("not enough available resources")]
    InsufficientResources,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ConnectWorkerResponse;

#[derive(Debug, Serialize, Deserialize, Error)]
pub enum ConnectWorkerError {
    #[error("id already taken")]
    DuplicateId,
    #[error("could not connect")]
    Io(String),
}
