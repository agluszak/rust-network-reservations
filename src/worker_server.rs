use crate::protocol::Protocol;
use crate::{Resources, ServerId};
use std::net::TcpListener;

struct WorkerServer {
    id: ServerId,
    available_resources: Resources,
    listener: TcpListener,
    main_connection: Protocol<(), ()>,
}
