use crate::{Resources, ServerId};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Serialize, Deserialize)]
pub struct ReservationRequest(pub Resources);

#[derive(Debug, Serialize, Deserialize)]
pub enum ReservationResponse {
    Success(ReservedResources),
    Failure,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ReservedResources(HashMap<ServerId, Resources>);
