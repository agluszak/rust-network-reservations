use byteorder::{NetworkEndian, ReadBytesExt, WriteBytesExt};
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::fmt::Debug;
use std::io;

use std::io::{Read, Write};
use std::net::{Shutdown, SocketAddr, TcpStream};
use std::time::SystemTime;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum ProtocolSendError {
    #[error("io error")]
    Io(#[from] std::io::Error),
    #[error("send error")]
    ChannelSend,
}

#[derive(Debug, Error)]
pub enum ProtocolRecvError {
    #[error("io error")]
    Io(#[from] std::io::Error),
    #[error("receive error")]
    ChannelReceive(#[from] crossbeam_channel::RecvError),
    #[error("json error")]
    Json(#[from] serde_json::Error),
}

type SendResult<T> = std::result::Result<T, ProtocolSendError>;

type RecvResult<T> = std::result::Result<T, ProtocolRecvError>;

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
pub struct Message<MessageType> {
    pub message_id: u64,
    pub sender_id: u64,
    pub sent_at: SystemTime,
    pub payload: MessageType,
}

impl<MessageType> Message<MessageType> {
    pub fn new(
        message_id: u64,
        sender_id: u64,
        sent_at: SystemTime,
        message_type: MessageType,
    ) -> Self {
        Message {
            message_id,
            sender_id,
            sent_at,
            payload: message_type,
        }
    }
}

pub struct Protocol<InboundMessage, OutboundMessage> {
    protocol_impl: ProtocolImpl<InboundMessage, OutboundMessage>,
    message_counter: u64,
    id: u64,
}

enum ProtocolImpl<InboundMessage, OutboundMessage> {
    TcpProtocol(TcpStream),
    LocalProtocol(
        crossbeam_channel::Sender<Message<OutboundMessage>>,
        crossbeam_channel::Receiver<Message<InboundMessage>>,
    ),
}

impl<InboundMessage, OutboundMessage> Protocol<InboundMessage, OutboundMessage>
where
    InboundMessage: DeserializeOwned,
    OutboundMessage: Serialize + Debug,
{
    fn new(protocol_impl: ProtocolImpl<InboundMessage, OutboundMessage>, id: u64) -> Self {
        Protocol {
            protocol_impl,
            message_counter: 0,
            id,
        }
    }

    pub fn connect_tcp(addr: SocketAddr, id: u64) -> io::Result<Self> {
        let protocol_impl = ProtocolImpl::TcpProtocol(TcpStream::connect(addr)?);
        Ok(Protocol::new(protocol_impl, id))
    }

    pub fn from_tcp_stream(stream: TcpStream, id: u64) -> Self {
        Protocol::new(ProtocolImpl::TcpProtocol(stream), id)
    }

    pub fn recv(&mut self) -> RecvResult<Message<InboundMessage>> {
        match &mut self.protocol_impl {
            ProtocolImpl::TcpProtocol(ref mut stream) => {
                let message_length = stream.read_u64::<NetworkEndian>()?;
                let mut message_bytes = vec![0; message_length as usize];
                stream.read_exact(&mut message_bytes)?;
                let message: Message<InboundMessage> = serde_json::from_slice(&message_bytes)?;
                Ok(message)
            }
            ProtocolImpl::LocalProtocol(_, receiver) => {
                let message = receiver.recv()?;
                Ok(message)
            }
        }
    }

    pub fn send(&mut self, message: OutboundMessage) -> SendResult<()> {
        let now = SystemTime::now();
        let message = Message::new(self.message_counter, self.id, now, message);
        self.message_counter += 1;
        match &mut self.protocol_impl {
            ProtocolImpl::TcpProtocol(ref mut stream) => {
                let message_bytes = serde_json::to_vec(&message)
                    .unwrap_or_else(|_| panic!("json error - failed to serialize {:?}", message));
                let message_length = message_bytes.len() as u64;
                stream.write_u64::<NetworkEndian>(message_length)?;
                stream.write_all(&message_bytes)?;
                Ok(())
            }
            ProtocolImpl::LocalProtocol(sender, _) => {
                sender
                    .send(message)
                    .map_err(|_| ProtocolSendError::ChannelSend)?;
                Ok(())
            }
        }
    }
}

impl<InboundMessage, OutboundMessage> Drop for Protocol<InboundMessage, OutboundMessage> {
    fn drop(&mut self) {
        match &mut self.protocol_impl {
            ProtocolImpl::TcpProtocol(ref mut stream) => {
                stream.shutdown(Shutdown::Both).unwrap();
            }
            ProtocolImpl::LocalProtocol(_, _) => {}
        }
    }
}

impl<InboundMessage, OutboundMessage> Protocol<InboundMessage, OutboundMessage>
where
    InboundMessage: Serialize + DeserializeOwned + Debug,
    OutboundMessage: Serialize + DeserializeOwned + Debug,
{
    pub fn local(
        first_id: u64,
        second_id: u64,
    ) -> (
        Protocol<InboundMessage, OutboundMessage>,
        Protocol<OutboundMessage, InboundMessage>,
    ) {
        let (first_sender, first_receiver) = crossbeam_channel::unbounded();
        let (second_sender, second_receiver) = crossbeam_channel::unbounded();
        let first_impl = ProtocolImpl::LocalProtocol(first_sender, second_receiver);
        let second_impl = ProtocolImpl::LocalProtocol(second_sender, first_receiver);
        let first_protocol = Protocol::new(first_impl, first_id);
        let second_protocol = Protocol::new(second_impl, second_id);
        (first_protocol, second_protocol)
    }
}

mod tests {
    use super::*;
    use std::net::{Ipv4Addr, TcpListener};
    use std::thread::sleep;

    #[test]
    fn test_local() {
        let first_id = 10;
        let second_id = 177;
        let (mut first, mut second) = super::Protocol::local(first_id, second_id);

        let text = "Hello World!";
        first.send(text.to_string()).unwrap();
        let message = second.recv().unwrap();
        let expected_message = Message::new(0, first_id, message.sent_at, text.to_string());
        assert_eq!(message, expected_message);

        let text = "Hello World!";
        second.send(text.to_string()).unwrap();
        let message = first.recv().unwrap();
        let expected_message = Message::new(0, second_id, message.sent_at, text.to_string());
        assert_eq!(message, expected_message);

        let text = "How it's going?";
        first.send(text.to_string()).unwrap();
        let message = second.recv().unwrap();
        let expected_message = Message::new(1, first_id, message.sent_at, text.to_string());
        assert_eq!(message, expected_message);
    }

    #[test]
    fn test_tcp() {
        let server_id = 666;
        let client_id = 8;

        let localhost = [127, 0, 0, 1];
        let server_port = 6666;

        let text = "Hello World!";

        let handle = std::thread::spawn(move || {
            let listener = TcpListener::bind((Ipv4Addr::from(localhost), server_port)).unwrap();
            let (stream, _) = listener.accept().unwrap();
            let mut server = Protocol::from_tcp_stream(stream, server_id);
            let message: Message<String> = server.recv().unwrap();
            let expected_message = Message::new(0, client_id, message.sent_at, text.to_string());
            assert_eq!(message, expected_message);
            server.send(text.to_string()).unwrap();
            sleep(std::time::Duration::from_millis(100));
        });

        let mut client = Protocol::connect_tcp((localhost, server_port).into(), client_id).unwrap();
        client.send(text.to_string()).unwrap();
        let message: Message<String> = client.recv().unwrap();
        let expected_message = Message::new(0, server_id, message.sent_at, text.to_string());
        assert_eq!(message, expected_message);

        handle.join().unwrap();
    }
}
