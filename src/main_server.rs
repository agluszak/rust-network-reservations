use crate::main_messages::{
    ConnectWorkerError, ConnectWorkerRequest, ConnectWorkerResponse, MainRequestType,
    MainResponseType, ReservationError, ReservationRequest,
};
use crate::protocol::{Message, Protocol, ProtocolSendError};
use crate::{Resources, ServerId, TransactionId};
use std::collections::{HashMap, HashSet, VecDeque};

use std::sync::atomic::AtomicBool;
use std::sync::{Arc, Mutex};
use std::thread;
use std::thread::JoinHandle;
use tracing::{event, warn, Level};

struct Connection {
    cancelled: Arc<AtomicBool>,
    thread_handle: JoinHandle<()>,
    protocol: Arc<Mutex<Protocol<MainRequestType, MainResponseType>>>,
}

impl Connection {
    pub fn close(&self) {
        self.cancelled
            .store(true, std::sync::atomic::Ordering::Relaxed);
    }

    pub fn send(&mut self, msg: MainResponseType) -> Result<(), ProtocolSendError> {
        self.protocol.lock().unwrap().send(msg)
    }

    pub fn spawn(
        task_sender: crossbeam_channel::Sender<MainRequestType>,
        protocol: Protocol<MainRequestType, MainResponseType>,
    ) -> Connection {
        let protocol = Arc::new(Mutex::new(protocol));
        let protocol_clone = Arc::clone(&protocol);
        let cancelled = Arc::new(AtomicBool::new(false));
        let cancelled_clone = Arc::clone(&cancelled);
        let thread_handle = thread::spawn(move || {
            while !cancelled_clone.load(std::sync::atomic::Ordering::Relaxed) {
                let Message {
                    message_id,
                    sender_id,
                    sent_at,
                    payload,
                } = protocol_clone.lock().unwrap().recv().unwrap(); // TODO: handle error
                event!(
                    Level::INFO,
                    ?sender_id,
                    ?message_id,
                    ?sent_at,
                    ?payload,
                    "request",
                );
                task_sender.send(payload).unwrap(); // TODO: handle error
            }
        });
        Connection {
            cancelled,
            thread_handle,
            protocol,
        }
    }
}

struct MainServer {
    total_resources: Resources,
    server_resources: HashMap<ServerId, Resources>,
    connections: HashMap<ServerId, Connection>,
    task_queue: crossbeam_channel::Receiver<MainRequestType>,
    task_sender: crossbeam_channel::Sender<MainRequestType>,
}

enum ServerState {
    Reserving(TransactionId, HashSet<ServerId>),
    ShuttingDown,
    Ready,
}

impl MainServer {
    pub fn new() -> MainServer {
        let (task_sender, task_queue) = crossbeam_channel::unbounded();

        MainServer {
            total_resources: Resources::new(),
            server_resources: HashMap::new(),
            connections: HashMap::new(),
            task_queue,
            task_sender,
        }
    }

    pub fn run(&mut self) {
        let mut state = ServerState::Ready;
        let mut transaction = 0;
        let mut saved_for_later = VecDeque::new();

        loop {
            match state {
                ServerState::Ready => {
                    let message = if !saved_for_later.is_empty() {
                        saved_for_later.pop_front().unwrap()
                    } else {
                        self.task_queue.recv().unwrap()
                    };

                    match message {
                        MainRequestType::Reservation(request) => {
                            let transaction_id = TransactionId(transaction);
                            transaction += 1;
                            let reply_to = request.reply_to;

                            if self.connections.contains_key(&reply_to) {
                                let result =
                                    self.handle_reservation_request(request, transaction_id);
                                if let Ok(servers) = result {
                                    state = ServerState::Reserving(transaction_id, servers);
                                } else {
                                    let response = MainResponseType::Reservation(Err(
                                        ReservationError::InsufficientResources,
                                    ));
                                    let connection = self.connections.get_mut(&reply_to).unwrap();
                                    connection.send(response).unwrap();
                                }
                            } else {
                                warn!(
                                    // TODO
                                    // ?reply_to
                                    "connection request sent from unknown server",
                                );
                            }
                        }
                        MainRequestType::ConnectWorker(request) => {
                            let reply_to = request.reply_to;

                            if self.connections.contains_key(&reply_to) {
                                let response = self.handle_connection_request(request);
                                let connection = self.connections.get_mut(&reply_to).unwrap();
                                connection
                                    .send(MainResponseType::ConnectWorker(response))
                                    .unwrap();
                            } else {
                                warn!(
                                    // ?reply_to
                                    "connection request sent from unknown server",
                                );
                            }
                        }
                        MainRequestType::ReservationConfirmation(confirmation) => {
                            warn!(
                                ?confirmation.server_id,
                                ?confirmation.transaction_id,
                                "unexpected reservation confirmation",
                            );
                        }
                    }
                }
                ServerState::Reserving(transaction, mut expected_ids) => {
                    let message = self.task_queue.recv().unwrap();
                    if let MainRequestType::ReservationConfirmation(confirmation) = message {
                        if confirmation.transaction_id != transaction {
                            state = ServerState::Reserving(transaction, expected_ids);
                            warn!(
                                ?transaction,
                                ?confirmation.server_id,
                                ?confirmation.transaction_id,
                                "confirmation for wrong transaction",
                            );
                        } else if expected_ids.remove(&confirmation.server_id) {
                            if expected_ids.is_empty() {
                                state = ServerState::Ready;
                            } else {
                                state = ServerState::Reserving(transaction, expected_ids);
                            }
                        } else {
                            state = ServerState::Reserving(transaction, expected_ids);
                            warn!(
                                ?transaction,
                                ?confirmation.server_id,
                                ?confirmation.transaction_id,
                                "confirmation for wrong server",
                            );
                        }
                    } else {
                        saved_for_later.push_back(message);
                        state = ServerState::Reserving(transaction, expected_ids);
                    }
                }
                ServerState::ShuttingDown => {
                    todo!()
                }
            }
        }
    }

    fn handle_connection_request(
        &mut self,
        request: ConnectWorkerRequest,
    ) -> Result<ConnectWorkerResponse, ConnectWorkerError> {
        let ConnectWorkerRequest {
            worker_id,
            address,
            resources,
            ..
        } = request;
        let tcp_protocol = Protocol::connect_tcp(address, worker_id.0);
        match tcp_protocol {
            Ok(protocol) => self.connect_worker(worker_id, protocol, resources),
            Err(error) => Err(ConnectWorkerError::Io(error.to_string())),
        }
    }

    fn handle_reservation_request(
        &mut self,
        request: ReservationRequest,
        transaction_id: TransactionId,
    ) -> Result<HashSet<ServerId>, ReservationError> {
        todo!()
    }

    fn connect_worker(
        &mut self,
        id: ServerId,
        connection: Protocol<MainRequestType, MainResponseType>,
        resources: Resources,
    ) -> Result<ConnectWorkerResponse, ConnectWorkerError> {
        if self.connections.contains_key(&id) {
            return Err(ConnectWorkerError::DuplicateId);
        }
        self.total_resources.add(&resources);
        self.server_resources.insert(id, resources);
        self.connections
            .insert(id, Connection::spawn(self.task_sender.clone(), connection));
        Ok(ConnectWorkerResponse)
    }
}
